#!/bin/bash
#
# base9 build kickoff wrapper
#
###########################

unset LD_LIBRARY_PATH

get_make(){

which colormake

case $? in
	0)
         echo "Using colormake"
	 MAKE=$(which colormake)
	;;

	1)
	 echo "Making with make"
	 MAKE=$(which make)
	;;
esac
	
}

get_make

$MAKE
